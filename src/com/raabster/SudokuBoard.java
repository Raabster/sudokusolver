package com.raabster;

import java.util.Arrays;

class SudokuBoard {

    public boolean solve(int[][] board) {
        int row = -1;
        int column = -1;
        boolean noEmptyCells = true;

        outerloop:
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] == 0) {
                    row = i;
                    column = j;

                    noEmptyCells = false;
                    break outerloop;
                }
            }
        }

        if (noEmptyCells) {
            show(board);
            return true;
        }

        for (int number = 1; number <= 9; number++) {
            if (checkConstraints(row, column, number, board)) {
                board[row][column] = number;
                if (solve(board)) {
                    return true;
                } else {
                    board[row][column] = 0;
                }
            }
        }

        return false;
    }

    private boolean checkConstraints(int row, int column, int number, int[][] board) {
        if (validRow(row, number, column, board) && validColumn(column, number, row, board) && validSquare(row, column, number, board)) return true;
        return false;
    }

    private boolean validRow(int row, int number, int column, int[][] board) {
        for (int i = 0; i < board.length; i++) {
            if (i == column) continue;
            if (number == board[row][i]) return false;
        }
        return true;
    }

    private boolean validColumn(int column, int number, int row, int[][] board) {
        for (int i = 0; i < board.length; i++) {
            if (i == row) continue;
            if (number == board[i][column]) return false;
        }
        return true;
    }

    private boolean validSquare(int row, int column, int number, int[][] board) {
        int start_row = row - (row % 3);
        int start_column = column - (column % 3);
        for (int i = start_row; i < (start_row + 3); i++) {
            for (int j = start_column; j < (start_column + 3); j++) {
                if (i == row && j == column) continue;
                if (number == board[i][j]) return false;
            }
        }
        return true;
    }

    public void show(int[][] board) {
        for (int[] row : board) {
            System.out.println(Arrays.toString(row));
        }
    }
}
