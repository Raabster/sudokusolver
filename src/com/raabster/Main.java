package com.raabster;

public class Main {

    public static void main(String[] args) {
        SudokuBoard sudoku_board = new SudokuBoard();
        /*int[][] random_game = new int[][] {
                {0,0,7,0,0,0,9,0,0},
                {0,0,0,0,1,2,0,8,0},
                {5,4,0,0,0,7,2,0,1},
                {4,0,3,9,0,8,7,0,0},
                {6,0,9,0,5,0,4,0,8},
                {0,0,2,7,0,4,6,0,9},
                {0,3,0,5,2,0,0,0,0},
                {7,0,1,8,0,0,0,9,5},
                {0,0,5,0,0,0,8,0,0},
        };*/
        int[][] random_game = new int[][] {
                {0,8,0,0,2,0,5,6,0},
                {0,0,0,1,0,0,0,0,7},
                {0,1,0,5,0,0,0,0,9},
                {0,5,0,0,9,0,4,0,8},
                {0,0,7,8,0,0,0,0,3},
                {0,9,0,0,1,0,0,5,0},
                {2,0,4,0,0,0,8,0,0},
                {0,6,0,0,8,5,0,0,0},
                {0,0,0,2,0,0,1,0,0}
        };

        sudoku_board.solve(random_game);
    }
}
